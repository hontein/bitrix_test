<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Сервис по определению CMS битрикс</title>

    <!-- Bootstrap -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="/js/jquery.min.js"></script>
</head>
<body>

<h1 class="text-center">Сервис по определению CMS битрикс</h1>
<div class="container">
    <div class="row">
        <div class="col-sm-4 ">Пожалуйста, выберите файл с расширением xlsx/xls:
        </div>
        <div class="col-sm-4 form-group">
            <label id="btn-selector" class="btn btn-primary" for="my-file-selector" style="width: 200px;">
                <input accept="application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                       id="my-file-selector" type="file" style="display:none"
                       onchange="$('#upload-file-info').html(this.files[0].name); $('#upload_file').removeClass('hidden'); $('#block_progressbar').addClass('hidden');">
                Выбрать файл
            </label>
            <span class='label label-info' id="upload-file-info"></span>

        </div>
        <div class="col-sm-3">
            <label id="upload_file" class="btn btn-info hidden" style="width: 200px;">
                Загрузить
            </label>
        </div>
    </div>
    <!--    <div class="row">-->
    <!--        <div class="col-sm-3 ">-->
    <!--            Скачать модифицированный файл:-->
    <!--        </div>-->
    <!--        <div class="col-sm-3 form-group">-->
    <!--            <label id="download_btn" class="btn btn-primary " style="width: 200px;">-->
    <!--                Скачать файл-->
    <!--            </label>-->
    <!--        </div>-->
    <!--    </div>-->
    <div id="block_progressbar" class="progress hidden" style="width: 80%;">
        <div id="theprogressbar" class="progress-bar-success " role="progressbar" aria-valuenow="0" aria-valuemin="0"
             aria-valuemax="100"
             style="width: 0%;">
            <span id="percent_complete"></span>...Идет обработка файла...
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#upload_file').click(function () {
            $('#block_progressbar').removeClass('hidden');
            $('#upload_file').addClass('disabled');
            $('#btn-selector').addClass('disabled');
            send_file();

        });


        function send_file() {
            var $input = $("#my-file-selector");
            var fd = new FormData;
            fd.append("excel", $input.prop('files')[0]);
            $.ajax({
                url: 'file_upload.php',
                data: fd,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                    action(0, $("#my-file-selector").prop('files')[0]['name']);
                }
            });

        }

        function action(start, filename) {
            $.ajax({
                url: 'action.php',
                type: 'POST',
                data: {'start': start, 'filename': filename},
                success: function (data) {
                    var result = JSON.parse(data);
                    if (result[0] < result[1]) {
                        var percent = Math.round((result[0] * 100) / result[1]);
                        $('#theprogressbar').attr('aria-valuenow', percent).css('width', percent + '%');
                        $('#percent_complete').text(percent + '%');
                        $('#block_progressbar').removeClass('hidden');
                        action(result[0],$("#my-file-selector").prop('files')[0]['name']);
                    }
                    if (result[0] == result[1]) {
                        $('#theprogressbar').attr('aria-valuenow', 100).css('width', '100%');
                        $('#percent_complete').text(100 + '%');
                        $('#upload_file').removeClass('disabled');
                        $('#btn-selector').removeClass('disabled');
                        window.location.href = $("#my-file-selector").prop('files')[0]['name'];
                    }
                }
            });
        }
    });
</script>

<script src="/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
