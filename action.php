<?

if ( ! is_numeric( $_POST['start'] ) ) {
	die;
}
$start_index = $_POST['start'];
$filename = $_POST['filename'];

set_time_limit( 0 );
ini_set( 'default_socket_timeout', 1 );

function open_url( $url ) {
	$url_c = parse_url( $url );

	$url_admin_bitrix = $url_c['scheme'] . '://' . $url_c['host'] . "/bitrix/admin/index.php";

	$ch = curl_init( $url_admin_bitrix );
	curl_setopt( $ch, CURLOPT_HEADER, 1 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
	curl_exec( $ch );
	$info = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
	curl_close( $ch );

	if ( in_array( $info, [ 200, 301, 302 ])  ) {

		$content = @file_get_contents( $url_admin_bitrix );

		if ( strpos( $content, "/bitrix/panel/" ) ) {
			return true;
		}
	}

	return false;
}

require_once( 'Classes/PHPExcel/IOFactory.php' );
require_once( 'Classes/PHPExcel/Writer/Excel2007.php' );

$xlsx = PHPExcel_IOFactory::load( $filename );

$xlsx->setActiveSheetIndex( 0 );

$sheet = $xlsx->getActiveSheet();

$rowIterator = $sheet->getColumnIterator( 'C', 'C' );
foreach ( $rowIterator as $row ) {
	// Получили ячейки текущей строки и обойдем их в цикле
	$cellIterator = $row->getCellIterator( 2 );

	foreach ( $cellIterator as $cell ) {
		if ( ! is_null( $cell->getCalculatedValue() ) ) {
			$arURL[] = $cell->getCalculatedValue();
		}
	}

}

$count_record = count( $arURL );

$sheet->setCellValue( 'F1', 'Битрикс?' );
$i = $start_index;
while ( $i < $count_record ) {

	if ( open_url( $arURL[ $i ] ) ) {
		$sheet->setCellValue( 'F' . ($i+2), 'Да' );
	} else {
		$sheet->setCellValue( 'F' . ($i+2), 'Нет' );
	}
	$i ++;
	if ( ( $i % 10 == 0 ) || ( $count_record == $i ) ) {
		$start_index = $i;
		break;
	}

}

$objWriter = PHPExcel_IOFactory::createWriter( $xlsx, 'Excel2007' );

$objWriter->save( $filename );
echo json_encode( [ $start_index, $count_record ] );
